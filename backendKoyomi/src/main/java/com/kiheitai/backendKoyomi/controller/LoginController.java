package com.kiheitai.backendKoyomi.controller;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.kiheitai.backendKoyomi.constant.ViewConstant;
import com.kiheitai.backendKoyomi.model.CredencialesUsuario;

@Controller
public class LoginController {
	
	private static final Log LOG = LogFactory.getLog(LoginController.class);
	
	@GetMapping("/")
	public String redirigirALogin() {
		LOG.info("metodoRedirigirLogin");
		return "redirect:/login";
	}
	
	@GetMapping("/login")
	public String showLoginForm(Model model,
			@RequestParam(name="error",required=false) String error,
			@RequestParam(name="logout",required=false)String logout){
		LOG.info("metodoShowLoginGorm con :: "+ error+"::  LOGOUT ::"+logout);
		model.addAttribute("error",error);
		model.addAttribute("logout",logout);
		model.addAttribute("credencialesUsuario",new CredencialesUsuario());
		LOG.info("Retornando vista login");
		return ViewConstant.LOGIN;
	}
	
	@PostMapping("/loginCheck")
	public String loginCheck(@ModelAttribute(name="credencialesUsuario")CredencialesUsuario credencialesUsuario) {
		LOG.info("metodologinCheck con :: "+ credencialesUsuario.toString());
		if((credencialesUsuario.getUsername().equals("user")) && (credencialesUsuario.getPassword().equals("user"))) {
			LOG.info("Retornando vista contactos");
			return "redirect:/contactos/mostarContactos";
		}
		LOG.info("Retornando vista login?error");

		return "redirect:/login?error";
	}
}
