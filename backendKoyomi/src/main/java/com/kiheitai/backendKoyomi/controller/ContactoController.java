package com.kiheitai.backendKoyomi.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.kiheitai.backendKoyomi.constant.ViewConstant;
import com.kiheitai.backendKoyomi.entity.Contacto;
import com.kiheitai.backendKoyomi.model.ContactoModel;
import com.kiheitai.backendKoyomi.service.ContactoService;

@Controller
@RequestMapping("/contactos")
public class ContactoController {

	private static final Log LOG=LogFactory.getLog(ContactoController.class);
	
	@Autowired
	@Qualifier("contactoServiceImpl")
	private ContactoService contactoService;
	
	@GetMapping("/cancel")
	public String Cancelar() {
		return "redirect:/contactos/mostarContactos";
	}
	
	@GetMapping("/contactoform")
	private String redirectContactoForm(@RequestParam(name="id",required=false) int id,
			Model model) {
		ContactoModel contactoModel = new ContactoModel();
		if(id !=0) {
			contactoModel = contactoService.buscarContactoModelPorId(id);
		}
		model.addAttribute("contactoModel",contactoModel);
		return ViewConstant.CONTACT_FORM;
	}
	
	@PostMapping("/addcontact")
	public String agregarContacto(@ModelAttribute(name="contactoModel") ContactoModel contactoModel,
			Model model) {
		LOG.info("metodoaddContact con :: "+ contactoModel.toString());
		if(null !=contactoService.añadirContacto(contactoModel)) {
			model.addAttribute("result",1);
		}else {
			model.addAttribute("result",0);
		}
		return "redirect:/contactos/mostarContactos";
	}
	
	@GetMapping("/mostarContactos")
	public ModelAndView mostrarContactos() {
		ModelAndView mav = new ModelAndView(ViewConstant.CONTACTS);
		mav.addObject("contactos",contactoService.listarContactos());
		return mav;
	}
	
	@GetMapping("/eliminarContacto")
	public ModelAndView eliminarContacto(@RequestParam(name="id",required=true)int id) {
		contactoService.eliminarContacto(id);
		return mostrarContactos();
	}
	
}
