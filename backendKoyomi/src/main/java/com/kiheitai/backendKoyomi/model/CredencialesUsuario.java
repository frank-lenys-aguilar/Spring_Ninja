package com.kiheitai.backendKoyomi.model;

public class CredencialesUsuario {
	private String username;
	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public CredencialesUsuario(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}

	public CredencialesUsuario() {
		super();
	}

	@Override
	public String toString() {
		return "CredencialesUsuario [username=" + username + ", password=" + password + "]";
	}

	
}
