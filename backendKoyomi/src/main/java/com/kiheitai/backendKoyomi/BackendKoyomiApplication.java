package com.kiheitai.backendKoyomi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class BackendKoyomiApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(BackendKoyomiApplication.class);
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(BackendKoyomiApplication.class, args);
    }

}
/*
@SpringBootApplication
public class BackendKoyomiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendKoyomiApplication.class, args);
	}
}
*/