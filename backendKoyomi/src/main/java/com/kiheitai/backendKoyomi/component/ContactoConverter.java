package com.kiheitai.backendKoyomi.component;

import org.springframework.stereotype.Component;

import com.kiheitai.backendKoyomi.entity.Contacto;
import com.kiheitai.backendKoyomi.model.ContactoModel;

@Component("contactoConverter")
public class ContactoConverter {

	public Contacto convertirModelContactAContacto(ContactoModel contactoModel) {
		Contacto contacto = new Contacto();
		contacto.setApellido(contactoModel.getApellido());
		contacto.setCiudad(contactoModel.getCiudad());
		contacto.setId(contactoModel.getId());
		contacto.setNombre(contactoModel.getNombre());
		contacto.setTelefono(contactoModel.getTelefono());
		return contacto;
	}
	
	public ContactoModel convertirContactoAContactoModel(Contacto contacto) {
		ContactoModel contactoModel = new ContactoModel();
		contactoModel.setApellido(contacto.getApellido());
		contactoModel.setCiudad(contacto.getCiudad());
		contactoModel.setId(contacto.getId());
		contactoModel.setNombre(contacto.getNombre());
		contactoModel.setTelefono(contacto.getTelefono());
		return contactoModel;
	}
}
