package com.kiheitai.backendKoyomi.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kiheitai.backendKoyomi.entity.Contacto;

@Repository("contactoRepository")
public interface ContactoRepository extends JpaRepository<Contacto, Serializable> {
	public abstract Contacto findById(int id);
}
