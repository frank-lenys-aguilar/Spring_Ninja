package com.kiheitai.backendKoyomi.service;

import java.util.List;

import com.kiheitai.backendKoyomi.entity.Contacto;
import com.kiheitai.backendKoyomi.model.ContactoModel;

public interface ContactoService {
	
	public abstract ContactoModel añadirContacto(ContactoModel contactoModel);
	
	public abstract List<ContactoModel> listarContactos();

	public abstract Contacto buscarContactoPorId(int id);
	
	public abstract void eliminarContacto(int id);
	public ContactoModel buscarContactoModelPorId(int id) ;
	
}
