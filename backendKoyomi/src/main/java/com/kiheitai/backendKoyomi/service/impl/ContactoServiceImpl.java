package com.kiheitai.backendKoyomi.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.kiheitai.backendKoyomi.component.ContactoConverter;
import com.kiheitai.backendKoyomi.entity.Contacto;
import com.kiheitai.backendKoyomi.model.ContactoModel;
import com.kiheitai.backendKoyomi.repository.ContactoRepository;
import com.kiheitai.backendKoyomi.service.ContactoService;

@Service("contactoServiceImpl")
public class ContactoServiceImpl implements ContactoService{

	@Autowired
	@Qualifier("contactoRepository")
	private ContactoRepository contactoRepository;
	
	@Autowired
	@Qualifier("contactoConverter")
	private ContactoConverter contactoConverter;
	
	
	@Override
	public ContactoModel añadirContacto(ContactoModel contactoModel) {
		Contacto contacto = contactoRepository.save(contactoConverter.convertirModelContactAContacto(contactoModel));
		
		return contactoConverter.convertirContactoAContactoModel(contacto);
	}


	@Override
	public List<ContactoModel> listarContactos() {
		List<Contacto> contactos =contactoRepository.findAll();
		List<ContactoModel> contactosModel = new ArrayList<ContactoModel>();
		for(Contacto contacto : contactos) {
			contactosModel.add(contactoConverter.convertirContactoAContactoModel(contacto));
		}
		return contactosModel;
	}


	@Override
	public Contacto buscarContactoPorId(int id) {
		return contactoRepository.findById(id);
		
	}
	
	public ContactoModel buscarContactoModelPorId(int id) {
		return contactoConverter.convertirContactoAContactoModel(buscarContactoPorId(id));
	}

	@Override
	public void eliminarContacto(int id) {
		Contacto contacto= buscarContactoPorId(id);
		if(null!=contacto) {
			contactoRepository.delete(contacto);
		}
		
	}

}
